package pl.org.openchessdb.repository.enums

enum class ChessResult {
    WHITE_WINS, BLACK_WINS, DRAW, UNSPECIFIED
}
