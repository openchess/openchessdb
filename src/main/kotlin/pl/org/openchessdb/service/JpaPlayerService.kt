package pl.org.openchessdb.service

import org.springframework.stereotype.Service
import pl.org.openchessdb.repository.PlayerRepository
import pl.org.openchessdb.repository.entity.Player
import javax.transaction.Transactional

@Transactional
@Service internal class JpaPlayerService(val playerRepository: PlayerRepository) : PlayerService {
    override fun addPlayer(fideId: Long, name: String) {
        playerRepository.save(Player(fideId = fideId, name=name))
    }
}
