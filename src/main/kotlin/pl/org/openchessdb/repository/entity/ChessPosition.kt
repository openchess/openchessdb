package pl.org.openchessdb.repository.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import org.hibernate.id.enhanced.SequenceStyleGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.AUTO
import javax.persistence.Id

@Entity
data class ChessPosition (
    @GeneratedValue(strategy = AUTO, generator = "chessposition_id_seq")
    @GenericGenerator(name = "chessposition_id_seq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = arrayOf(
                    Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "chessposition_id_seq"),
                    Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            ))
    @Id @Column val id: Long? = null,
    @Column(nullable = false, length = 100) val position: String = ""
)
